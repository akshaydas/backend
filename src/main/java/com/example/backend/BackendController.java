package com.example.backend;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BackendController {
	
	@GetMapping("/hello")
	public String hello() {

		return "b/e service up and running";
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<BackendBean> getTodoList(@PathVariable int id) {

		return new ResponseEntity<>(dummyValue(), HttpStatus.OK);

	}

	private BackendBean dummyValue() {
		BackendBean backendBean = new BackendBean();
		backendBean.setId(1);
		backendBean.setName("Sarath ts");
		return backendBean;

	}

}
