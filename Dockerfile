FROM openjdk:8-jdk-alpine

EXPOSE 8124

COPY target/backend.jar backend.jar

ENTRYPOINT ["java", "-jar", "backend.jar"]